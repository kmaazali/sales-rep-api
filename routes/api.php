<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'auth'], function(Router $api) {
        $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $api->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');

        $api->post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');

        $api->post('logout', 'App\\Api\\V1\\Controllers\\LogoutController@logout');
        $api->post('refresh', 'App\\Api\\V1\\Controllers\\RefreshController@refresh');
        $api->get('me', 'App\\Api\\V1\\Controllers\\UserController@me');
    });
    $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
        $api->get('protected', function() {
            return response()->json([
                'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.'
            ]);
        });
        $api->post('addclient', 'App\Http\Controllers\SalesManController@addclient');
        $api->get('getclients/{id}','App\Http\Controllers\SalesManController@showclients');
        $api->get('getcompanies','App\Http\Controllers\SalesManController@getCompanies');
        $api->get('getproducts/{id}','App\Http\Controllers\SalesManController@getProducts');
        $api->get('orders','App\Http\Controllers\SalesManController@getOrders');
        $api->post('addorder','App\Http\Controllers\SalesManController@addOrderItemsNew');
        $api->get('deleteorder/{id}','App\Http\Controllers\SalesManController@deleteOrder');



        $api->get('refresh', [
            'middleware' => 'jwt.refresh',
            function() {
                return response()->json([
                    'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!'
                ]);
            }
        ]);
    });

    $api->group(['middleware' => 'admin'], function(Router $api) {
        $api->get('companies','App\Http\Controllers\AdminController@getCompanies');
        $api->post('addcompany','App\Http\Controllers\AdminController@addCompany');
        $api->get('deletecompany/{id}','App\Http\Controllers\AdminController@deleteCompany');
        $api->post('updatecompany','App\Http\Controllers\AdminController@updateCompany');
        $api->get('products/{id}','App\Http\Controllers\AdminController@getProducts');
        $api->post('addproducts','App\Http\Controllers\AdminController@addProducts');
        $api->get('deleteproducts/{id}','App\Http\Controllers\AdminController@deleteProducts');
        $api->post('updateproducts','App\Http\Controllers\AdminController@updateProducts');
        $api->post('salesman','App\Http\Controllers\AdminController@addSalesMan');
        $api->get('deletesalesman/{id}','App\Http\Controllers\AdminController@deleteSalesman');
        $api->post('updatesalesman','App\Http\Controllers\AdminController@updateSalesman');
        $api->post('updatesalesmanpassword','App\Http\Controllers\AdminController@updateSalesmanPassword');
        $api->get('get/salesman','App\Http\Controllers\AdminController@getSalesman');
    });
    $api->get('hello', function() {
        return response()->json([
            'message' => 'This is a simple example of item returned by your APIs. Everyone can see it.'
        ]);
    });
});


