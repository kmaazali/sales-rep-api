<?php

namespace App\Http\Controllers;

use App\Company;
use App\OrderItems;
use App\Products;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Orders;
use App\Clients;

class AdminController extends Controller
{
    //
    public function admin(){
        return response()->json(['success'=>'true'],200);
    }

    public function getCompanies(){
        $user_id = Auth::user()->id;
        $companies = Company::where('user_id',$user_id)->get();

        return response()->json(['success'=>true,'companies'=>$companies],200);
    }

    public function addCompany(Request $request){
        if($request->has('company_name')&&$request->has('company_description')
            &&$request->hasFile('company_image')&&$request->has('company_phone')&&$request->has('company_address')
            &&$request->has('company_email')){
            $user_id = Auth::user()->id;
            $company = new Company ;
            $company->company_name = $request->company_name; //all fillable
            $company->company_description = $request->company_description;
            $file = $request->file('company_image');

            $name = "/uploads/".time() . '.' . $file->getClientOriginalExtension();

            $request->file('company_image')->move("uploads", $name);
            $company->company_image = $name;
            $company->company_phone = $request->company_phone;
            $company->company_address = $request->company_address;
            $company->user_id = $user_id;
            $company->company_email = $request->company_email;
            $company->save();
            return response()->json(['success' => true, 'message' => 'Company Added'], 200);}
        else{
            return response()->json(['success' => false, 'message' => 'Please fill all required fields'], 200);
        }

    }

    public function deleteCompany($id){
        //OrderItems::where('company_id',$id)->delete();
        $orders=Orders::where('company_id',$id)->get();
        foreach ($orders as $item) {
            OrderItems::where('order_id',$item['id'])->delete();
        }
        Orders::where('company_id',$id)->delete();
        Clients::where('company_id',$id)->delete();
        Products::where('company_id',$id)->delete();
        Company::where('id',$id)->delete();
        return response()->json(['success'=>true,'message'=> 'Company Deleted successfully'],200);

    }
    public function updateCompany(Request $request ){

        if ($request->has('company_id')&&$request->has('company_name')&&$request->has('company_description')
            &&$request->hasFile('company_image') &&$request->has('company_phone')&&$request->has('company_address')
            &&$request->has('company_email'))
        {
            $company_id=Company::where('id',$request->company_id)->get();
            $file = $request->file('company_image');

            $name = "/uploads/".time() . '.' . $file->getClientOriginalExtension();

            $request->file('company_image')->move("uploads", $name);

            if(!$company_id->isEmpty()){
                Company::where('id',$request['company_id'])->update(['company_name'=>$request->company_name,
                    'company_description'=>$request->company_description,'company_image'=>$name,
                    'company_phone'=>$request->company_phone,'company_address'=>$request->company_address,
                    'company_email'=>$request->company_email]);
                return response()->json(['success'=>true,'message'=> 'Company Updated successfully'],200);
            }else{
                return response()->json(['success'=>false,'message'=> 'Invalid Company'],200);

            }


        }
        else {
            return response()->json(['success' => false, 'message' => 'Unable To Update Company'], 200);
        }
    }

    public function getProducts($id){
        $products=Products::where('company_id',$id)->get();
        return response()->json(['success'=>true,'products'=>$products],200);
    }

    public function addProducts(Request $request){
    if($request->has('name')&&$request->hasFile('picture')
        &&$request->has('unit_price')&&$request->has('quantity')&&$request->has('company_id')){
        $user_id = Auth::user()->id;
        $products = new Products ;
        $products->name = $request->name; //all fillable
        $file = $request->file('picture');

        $name = "/uploads/".time() . '.' . $file->getClientOriginalExtension();

        $request->file('picture')->move("uploads", $name);
        $products->picture = $name;
        $products->unit_price = $request->unit_price;
        $products->company_id = $request->company_id;
        $products->quantity = $request->quantity;
        $products->user_id = $user_id;
        $products->save();
        return response()->json(['success' => true, 'message' => 'Product Added'], 200);}
    else{
        return response()->json(['success' => false, 'message' => 'Please fill all required fields'], 200);
    }

}
    public function updateProducts(Request $request ){

        if ($request->has('product_id')&&$request->has('name')&&$request->hasFile('picture')
            &&$request->has('unit_price')&&$request->has('quantity'))
        {
            $product_id=Products::where('id',$request->product_id)->get();

            $file = $request->file('picture');

            $name = "/uploads/".time() . '.' . $file->getClientOriginalExtension();

            $request->file('picture')->move("uploads", $name);

            if(!$product_id->isEmpty()){
                Products::where('id',$request['product_id'])->update(['name'=>$request->name,
                    'picture'=>$name,'unit_price'=>$request->unit_price,'quantity'=>$request->quantity]);
                return response()->json(['success'=>true,'message'=> 'Product Updated successfully'],200);
            }else{
                return response()->json(['success'=>false,'message'=> 'Invalid Product'],200);

            }


        }
        else {
            return response()->json(['success' => false, 'message' => 'Unable To Update Company'], 200);
        }
    }

    public function deleteProducts($id){
        $orderitems=OrderItems::where('product_id',$id)->get();
        foreach($orderitems as $item){
            OrderItems::where('order_id',$item['order_id'])->delete();
            Orders::where('id',$item['order_id'])->delete();
        }
        Products::where('id',$id)->delete();
        return response()->json(['success'=>true,'message'=> 'Product Deleted successfully'],200);

    }

    public function addSalesMan(Request $request){
        $user_id = Auth::user()->id;
        if($request->has('name')&&$request->has('email')&&$request->has('password')){
            $salesman = new User ;
            $salesman->name = $request->name; //all fillable
            $salesman->email = $request->email;
            $salesman->password = $request->password;
            $salesman->role_id=2;
            $salesman->created_by=$user_id;
            $salesman->save();
            return response()->json(['success' => true, 'message' => 'Salesman Added'], 200);}
        else{
            return response()->json(['success' => false, 'message' => 'Please fill all required fields'], 200);
        }

    }
    public function deleteSalesman($id){
        User::where('id',$id)->delete();
        return response()->json(['success'=>true,'message'=> 'Salesman Deleted successfully'],200);

    }
    public function updateSalesman(Request $request ){
        if ($request->has('name')&&$request->has('email')&&$request->has('id'))
        {
            $user_id=User::where('id',$request->id)->get();

            if(!$user_id->isEmpty()){
                User::where('id',$request['id'])->update(['name'=>$request->name,'email'=>$request->email]);
                return response()->json(['success'=>true,'message'=> 'Salesman Updated successfully'],200);
            }else{
                return response()->json(['success'=>false,'message'=> 'Invalid User'],200);

            }
        }
        else {
            return response()->json(['success' => false, 'message' => 'Unable To Update Salesman'], 200);
        }
    }

    public function updateSalesmanPassword(Request $request){
        if ($request->has('id')&&$request->has('password')) {
            $user_id = User::where('id', $request->id)->get();

            if (!$user_id->isEmpty()) {
                User::where('id',$request->id)->update(['password'=>bcrypt($request['password'])]);
                return response()->json(['success'=>true,'message'=>'Password Updated Successfully'],200);
            }
            else{
                return response()->json(['success'=>false,'message'=>'Unable To Update Password'],200);
            }
        }
    }

    public function getSalesman(){
        $salesman=User::where('role_id',2)->get();
        return response()->json(['success'=>true,'salesman'=>$salesman],200);
    }

}
