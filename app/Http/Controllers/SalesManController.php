<?php

namespace App\Http\Controllers;

use App\Company;
use App\OrderItems;
use App\Orders;
use Illuminate\Http\Request;
use App\Clients;
use App\User;
use App\Products;
use Auth;
use DB;

class SalesManController extends Controller
{
    public function  addclient(Request $request){
        if ($request->has('business_name')&&$request->has('client_first_name')&&$request->has('client_last_name')&&$request->has('phone_number')
            &&$request->has('fax')&&$request->has('email_address')){
            $clients = new Clients ;
            $clients->business_name = $request->business_name; //all fillable
            $clients->client_first_name = $request->client_first_name;
            $clients->client_last_name = $request->client_last_name;
            $clients->phone_number = $request->phone_number;
            $clients->fax = $request->fax;
            $clients->email_address = $request->email_address;
            $user_id = Auth::user()->id;
            $clients->created_by = $user_id;
            $clients->company_id=$request->company_id;
            $clients->save();
            return response()->json(['success' => true, 'message' => 'Client Added'], 200);

        }
        else {
            return response()->json(['success' => false, 'message' => 'Please fill all required fields'], 200);
        }

    }
    public function showclients($id){
        $clients=Clients::where('company_id',$id)->get();
        return response()->json(['success'=>true,'clients'=>$clients],200);

    }

    public function getCompanies(){
        $companies=Company::get();
        return response()->json(['success'=>true,'companies'=>$companies],200);
    }
    public function getProducts($id){
        $products=Products::where('company_id',$id)->get();
        return response()->json(['success'=>true,'products'=>$products],200);
    }

//
//    public function addOrderNew(Request $request){
//        if($request->has('client_id')&&$request->has('company_id')&&$request->has('order_date')
//            &&$request->has('delivery_date')){
//            $order=new Orders();
//            $order->client_id=$request->client_id;
//            $order->user_id=Auth::user()->id;
//            $order->company_id=$request->company_id;
//            $order->order_date=$request->order_date;
//            $order->delivery_date=$request->delivery_date;
//            $order->save();
//            return response()->json(['success' => true, 'message' => 'Order Added','data'=>$order], 200);
//
//        }
//        else{
//            return response()->json(['success'=>false,'message'=>'Please Fill All The Required Fields'],200);
//        }
//    }
//
//
//
//    public function updateOrders(Request $request ){
//
//        if ($request->has('order_id')&&$request->has('client_id')&&$request->has('order_date')
//            &&$request->has('delivery_date')&&$request->has('order_status')
//            &&$request->has('company_id'))
//        {
//            $order_id=Orders::where('id',$request->order_id)->get();
//
//            if(!$order_id->isEmpty()){
//                Orders::where('id',$request['order_id'])->update(['client_id'=>$request->client_id,
//                    'user_id'=>$request->user_id,'order_date'=>$request->order_date,'delivery_date'=>$request->delivery_date,
//                    'order_status'=>$request->order_status,'company_id'=>$request->company_id]);
//                return response()->json(['success'=>true,'message'=> 'Order Updated successfully'],200);
//            }else{
//                return response()->json(['success'=>false,'message'=> 'Invalid Order'],200);
//
//            }
//
//        }
//        else {
//            return response()->json(['success' => false, 'message' => 'Unable To Update Order'], 200);
//        }
//    }
//
//    public function deleteOrders($id){
//        Products::where('order_id',$id)->delete();
//        return response()->json(['success'=>true,'message'=> 'Order Deleted successfully'],200);
//
//    }
//
//    public function getOrders($id){
//        $order=Orders::where('order_id',$id)->get();
//        return response()->json(['success'=>true,'order'=>$order],200);
//
//    }
//
//
//    public function addOrderItems(Request $request){
//        $user_id = Auth::user()->id;
//        if($request->has('order_item_id')&&$request->has('order_id')&&$request->has('product_id')
//            &&$request->has('quantity')){
//            $orderitem = new OrderItems() ;
//            $orderitem-> order_item_id= $request->order_item_id; //all fillable
//            $orderitem->order_id = $request->order_id;
//            $orderitem->product_id = $request->product_id;
//            $orderitem->role_id=2;
//            $orderitem->created_by=$user_id;
//            $orderitem->save();
//            return response()->json(['success' => true, 'message' => 'Order Items Added'], 200);}
//        else{
//            return response()->json(['success' => false, 'message' => 'Please fill all required fields'], 200);
//        }
//
//    }
//    public function deleteOrderItems($id){
//        User::where('id',$id)->delete();
//        return response()->json(['success'=>true,'message'=> 'Order Items Deleted successfully'],200);
//
//    }
//    public function updateOrderItems(Request $request ){
//        if ($request->has('name')&&$request->has('email')&&$request->has('password')&&$request->has('id'))
//        {
//            $user_id=User::where('id',$request->id)->get();
//
//            if(!$user_id->isEmpty()){
//                $update = User::where('id',$request['id'])->update(['name'=>$request->name,'email'=>$request->email,'password'=>$request->password]);
//                return response()->json(['success'=>true,'message'=> 'Salesman Updated successfully'],200);
//            }else{
//                return response()->json(['success'=>false,'message'=> 'Invalid User'],200);
//
//            }
//        }
//        else {
//            return response()->json(['success' => false, 'message' => 'Unable To Update Salesman'], 200);
//        }
//    }


    public function addOrderItemsNew(Request $request)
    {
        //return response()->json([ $data = json_encode($request,true)], 200);}
        json_decode($request);
        $order=new Orders();
        $order->client_id=$request->client_id;
        $order->user_id=Auth::user()->id;
        $order->company_id=$request->company_id;
        $order->order_date=$request->order_date;
        $order->delivery_date=$request->delivery_date;
        $order->order_date=$request->order_date;
        $order->order_status=0;
        $order->save();
        foreach($request->order_items as $item){
            $order_items=new OrderItems();
            $order_items->order_id=$order->id;

            $order_items->product_id=$item['product_id'];
            $order_items->quantity=$item['quantity'];
            $order_items->save();

            $product=Products::where('id',$item['product_id'])->first();

            $quantity=((int)$product->quantity)-((int)$item['quantity']);
            Products::where('id',$item['product_id'])->update(['quantity'=>$quantity]);



        }
        return response()->json(['success'=>true,'message'=>'Order Added'],200);
    }

    public function deleteOrder($id)
    {
        $order = Orders::where('id', $id)->first();

        if (!$order==null) {
            $orderItems=OrderItems::where('order_id',$order->id)->get();

            foreach($orderItems as $item){
                $product=Products::where('id',$item['product_id'])->first();
                $quantity=((int)$product->quantity)+((int)$item->quantity);
                Products::where('id',$item['product_id'])->update(['quantity'=>$quantity]);
            }

            OrderItems::where('order_id',$id)->delete();
            Orders::where('id',$id)->delete();
            return response()->json(['success'=>true,'message'=>'Order Successfully Deleted'],200);
        }
        else{
            return response()->json(['success'=>false,'message'=>'No Orders Found'],200);
        }
    }

    public function updateOrder(Request $request){
        json_decode($request);
        $orderitem=OrderItems::where('order_id',$request->order_id)->get();
        foreach ($orderitem as $item){

        }
    }
    public function getOrders(){
        $user_id=Auth::user()->id;
        //$orders=Orders::where('user_id',$user_id)->with('user')->get();
        $results=DB::select(DB::raw("SELECT * from orders inner join orderitems on orders.id=orderitems.order_id where orders.user_id=:user"),array('user'=>$user_id));
        return response()->json(['success'=>true,'data'=>$results],200);
        //return response()->json(['success'=>true,'data'=>$orders],200);
    }

}
