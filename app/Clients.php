<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    protected $table='clients';

    protected $fillable=['business_name','client_first_name','client_last_name','phone_number','fax','email_address','created_by','company_id'];

    public function role(){
        return $this->belongsTo('App\User','created_by','id');//defined role_id as fkey in user& id is pkey in Role
    }

    public function comp(){
        return $this->belongsTo('App\Company','company_id','id');//defined role_id as fkey in user& id is pkey in Role
    }
}
