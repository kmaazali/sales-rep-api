<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table='company';

    protected $fillable=['company_id','company_name','company_description','company_image','company_phone','company_address','company_email','user_id'];

    public function com(){
        return $this->belongsTo('App\User','user_id','id');//defined role_id as fkey in user& id is pkey in Role
    }
}
