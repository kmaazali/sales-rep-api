<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
    protected $table='orderitems';

    protected $fillable=['order_item_id','order_id','product_id','quantity'];

    public function order(){
        return $this->belongsTo('App\Orders','order_id','id');//defined role_id as fkey in user& id is pkey in Role
    }

    public function prducts(){
        return $this->belongsTo('App\Products','client_id','id');//defined role_id as fkey in user& id is pkey in Role
    }
}
