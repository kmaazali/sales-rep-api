<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table='products';

    protected $fillable=['name','picture','unit_price','quantity','company_id','user_id'];

    public function company(){
        return $this->belongsTo('App\Company','company_id','id');//defined role_id as fkey in user& id is pkey in Role
    }
    public function user(){
        return $this->belongsTo('App\User','user_id','id');//defined role_id as fkey in user& id is pkey in Role
    }

}
