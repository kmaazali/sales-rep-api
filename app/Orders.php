<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
     protected $table='orders';

     protected $fillable=['order_id','client_id','user_id','order_date','delivery_date','order_status','company_id'];

    public function user(){
        return $this->belongsTo('App\User','order_id','id');//defined role_id as fkey in user& id is pkey in Role
    }

    public function company(){
        return $this->belongsTo('App\Company','company_id','id');//defined role_id as fkey in user& id is pkey in Role
    }
}
