<?php

namespace App\Api\V1\Controllers;

use App\User;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Auth;

class LoginController extends Controller
{
    /**
     * Log the user in
     *
     * @param LoginRequest $request
     * @param JWTAuth $JWTAuth
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request, JWTAuth $JWTAuth)
    {
        $credentials = $request->only(['email', 'password']);

        try {
            $token = Auth::guard()->attempt($credentials);

            if(!$token) {
                return response()->json(['success'=>false,'message'=>'Invalid Credentials'],200);
            }

        } catch (JWTException $e) {
            return response()->json(['success'=>false,'message'=>'Invalid Credentials'],200);
        }
        $user=User::where('email',$request['email'])->get(); // check user email from Db

        return response()
            ->json([
                'success' => true,
                'token' => $token,
                'user' => $user, //get user
                'expires_in' => Auth::guard()->factory()->getTTL() * 60
            ]);
    }
}
